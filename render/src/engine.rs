// Copyright (c) 2021, BlockProject 2D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 2D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::string::String;
use std::vec::Vec;
use std::boxed::Box;

#[derive(Clone, Copy, Debug)]
pub enum VertexAttributeType
{
    Float,
    Float2,
    Float3,
    Float4,
    Uint,
    Uint2,
    Uint3,
    Uint4,
    Int,
    Int2,
    Int3,
    Int4
}

#[derive(Clone, Copy, Debug)]
pub enum TextureFormat
{
    RGBA8,
    RG8,
    R8,
    RGBAFloat,
    RGFloat,
    RFloat
}

#[derive(Clone, Copy, Debug)]
pub enum TextureFilterMode
{
    LinearMinMax,
    NearestMinMax,
    LinearMinNearestMax,
    NearestMinLinearMax
}

#[derive(Clone, Copy, Debug)]
pub enum TextureWrapMode
{
    Repeat,
    ClampToEdge,
    MirroredRepeat
}

#[derive(Clone, Copy, Debug)]
pub enum ShaderStage
{
    Vertex,
    Geometry,
    Pixel
}

pub trait Resource
{
    fn lock(&mut self);
    fn free(self);
}

pub trait Shader : Resource
{
    fn setf(&mut self, name: &str, value: f32);
    fn seti(&mut self, name: &str, value: i32);
    fn setu(&mut self, name: &str, value: u32);
    fn set_vec2f(&mut self, name: &str, value: (f32, f32));
    fn set_vec3f(&mut self, name: &str, value: (f32, f32, f32));
    fn set_vec4f(&mut self, name: &str, value: (f32, f32, f32, f32));
    fn set_vec2i(&mut self, name: &str, value: (i32, i32));
    fn set_vec3i(&mut self, name: &str, value: (i32, i32, i32));
    fn set_vec4i(&mut self, name: &str, value: (i32, i32, i32, i32));
    fn set_vec2u(&mut self, name: &str, value: (u32, u32));
    fn set_vec3u(&mut self, name: &str, value: (u32, u32, u32));
    fn set_vec4u(&mut self, name: &str, value: (u32, u32, u32, u32));
    fn set_mat2f(&mut self, name: &str, value: &[f32; 4]);
    fn set_mat3f(&mut self, name: &str, value: &[f32; 9]);
    fn set_mat4f(&mut self, name: &str, value: &[f32; 16]);
    fn lock_texture_slot(&mut self, id: u8);
}

pub trait Texture : Resource
{
    fn set_filter_mode(&mut self, filter_mode: TextureFilterMode);
    fn set_wrap_mode(&mut self, wrap_mode_w: TextureWrapMode, wrap_mode_h: TextureWrapMode);
    fn update(&mut self, data: &[u8]) -> Result<(), String>;
}

pub trait VertexBuffer : Resource
{
    fn update(&mut self, data: &[f32]) -> Result<(), String>;
    fn draw(&mut self);
}

pub trait RenderEngine
{
    fn get_name(&mut self) -> String;
    fn read_pixels(&mut self, width: u32, height: u32, buffer: &mut [u8]);
}

pub trait ShaderBuilder : Sized
{
    type TRenderEngine: RenderEngine;

    fn new(engine: &mut Self::TRenderEngine) -> Self;
    fn add_stage(self, stage: ShaderStage, source: Vec<String>) -> Self;
    fn add_attribute(self, name: &str) -> Self;
    fn build(self) -> Result<Box<dyn Shader>, String>;
}

pub trait TextureBuilder : Sized
{
    type TRenderEngine: RenderEngine;

    fn new(engine: &mut Self::TRenderEngine) -> Self;
    fn dynamic(self) -> Self;
    fn size(self, width: u32, height: u32) -> Self;
    fn format(self, format: TextureFormat) -> Self;
    fn filter_mode(self, filter_mode: TextureFilterMode) -> Self;
    fn wrap_mode(self, wrap_mode_w: TextureWrapMode, wrap_mode_h: TextureWrapMode) -> Self;
    fn data<I: IntoIterator<Item = u8>>(self, data: I) -> Self;
    fn build(self) -> Result<Box<dyn Texture>, String>;
}

pub trait VertexBufferBuilder : Sized
{
    type TRenderEngine: RenderEngine;

    fn new(engine: &mut Self::TRenderEngine) -> Self;
    fn dynamic(self) -> Self;
    fn size(self, vertex_count: u32) -> Self;
    fn data<I: IntoIterator<Item = f32>>(self, data: I) -> Self;
    fn add_attribute(self, typeasf: VertexAttributeType) -> Self;
    fn build(self) -> Result<Box<dyn VertexBuffer>, String>;
}
