// Copyright (c) 2021, BlockProject 2D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 2D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::string::String;
use std::vec::Vec;
use std::boxed::Box;
use gl::types::GLuint;
use gl::types::GLint;
use gl::types::GLenum;

use super::renderengine::GlRenderEngine;
use bp2d_render::engine::VertexAttributeType;
use bp2d_render::engine::VertexBufferBuilder;
use bp2d_render::engine::VertexBuffer;
use bp2d_render::engine::Resource;

pub struct GlVertexBuffer
{
    vbo: GLuint,
    vao: GLuint,
    vbotype: GLenum,
    vcount: u32,
    buffer_size: usize
}

impl GlVertexBuffer
{
    pub fn new(vbo: GLuint, vao: GLuint, vbotype: GLenum, vcount: u32, buffer_size: usize) -> GlVertexBuffer
    {
        return GlVertexBuffer
        {
            vbo: vbo,
            vao: vao,
            vbotype: vbotype,
            vcount: vcount,
            buffer_size: buffer_size
        };
    }
}

impl Resource for GlVertexBuffer
{
    fn free(self)
    {
        unsafe
        {
            gl::DeleteVertexArrays(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
        }
    }

    fn lock(&mut self)
    {
        unsafe
        {
            gl::BindVertexArray(self.vao);
        }
    }
}

impl VertexBuffer for GlVertexBuffer
{
    fn update(&mut self, data: &[f32]) -> Result<(), String>
    {
        if self.buffer_size != data.len() * 4
        {
            return Err(format!("Invalid size of initial data, expected {} byte(s), got {} byte(s)", self.buffer_size, data.len() * 4));
        }
        unsafe
        {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(gl::ARRAY_BUFFER, (4 * data.len()) as isize, data.as_ptr() as _, self.vbotype);
        }
        return Ok(());
    }

    fn draw(&mut self)
    {
        unsafe
        {
            gl::DrawArrays(gl::TRIANGLES, 0, self.vcount as i32);
        }
    }
}

pub struct GlVertexBufferBuilder
{
    vbotype: GLenum,
    vcount: u32,
    initial_data: Option<Vec<f32>>,
    attributes: Vec<VertexAttributeType>
}

fn vertex_attrib_type_to_gl(atype: &VertexAttributeType) -> (GLint, GLenum)
{
    return match atype
    {
        VertexAttributeType::Float => (1, gl::FLOAT),
        VertexAttributeType::Float2 => (2, gl::FLOAT),
        VertexAttributeType::Float3 => (3, gl::FLOAT),
        VertexAttributeType::Float4 => (4, gl::FLOAT),
        VertexAttributeType::Uint => (1, gl::UNSIGNED_INT),
        VertexAttributeType::Uint2 => (2, gl::UNSIGNED_INT),
        VertexAttributeType::Uint3 => (3, gl::UNSIGNED_INT),
        VertexAttributeType::Uint4 => (4, gl::UNSIGNED_INT),
        VertexAttributeType::Int => (1, gl::INT),
        VertexAttributeType::Int2 => (2, gl::INT),
        VertexAttributeType::Int3 => (3, gl::INT),
        VertexAttributeType::Int4 => (4, gl::INT)
    }
}

fn vertex_attrib_list_to_gl(lst: &Vec<VertexAttributeType>) -> (Vec<(GLuint, GLint, GLenum)>, u32)
{
    let mut v = Vec::new();
    let mut idx: GLuint = 0;
    let mut size: u32 = 0;

    for a in lst
    {
        let (s, t) = vertex_attrib_type_to_gl(a);
        v.push((idx, s, t));
        idx += 1;
        size += s as u32 * 4;
    }
    return (v, size);
}

impl VertexBufferBuilder for GlVertexBufferBuilder
{
    type TRenderEngine = GlRenderEngine;

    fn new(_: &mut Self::TRenderEngine) -> Self
    {
        return GlVertexBufferBuilder
        {
            vbotype: gl::STATIC_DRAW,
            vcount: 0,
            initial_data: None,
            attributes: Vec::new()
        };
    }

    fn dynamic(mut self) -> Self
    {
        self.vbotype = gl::STREAM_DRAW;
        return self;
    }

    fn size(mut self, vertex_count: u32) -> Self
    {
        self.vcount = vertex_count;
        return self;
    }

    fn data<I: IntoIterator<Item = f32>>(mut self, data: I) -> Self
    {
        self.initial_data = Some(data.into_iter().collect());
        return self;
    }

    fn add_attribute(mut self, atype: VertexAttributeType) -> Self
    {
        self.attributes.push(atype);
        return self;
    }

    fn build(self) -> Result<Box<dyn VertexBuffer>, String>
    {
        let mut vbo: GLuint = 0;
        let mut vao: GLuint = 0;
        if self.vcount == 0
        {
            return Err(String::from("Cannot create a vertex buffer with 0 vertices"));
        }
        let (attrlist, size) = vertex_attrib_list_to_gl(&self.attributes);
        if let Some(v) = &self.initial_data
        {
            if (size * self.vcount) as usize != v.len() * 4
            {
                return Err(format!("Invalid size of initial data, expected {} byte(s), got {} byte(s)", (size * self.vcount) as usize, v.len() * 4));
            }
        }
        unsafe
        {
            gl::GenBuffers(1, &mut vbo);
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            if let Some(v) = self.initial_data
            {
                gl::BufferData(gl::ARRAY_BUFFER, (v.len() * 4) as isize, v.as_ptr() as _, self.vbotype);
            }
            else
            {
                let empty: Vec<u8> = vec![0; (size * self.vcount) as usize];
                gl::BufferData(gl::ARRAY_BUFFER, empty.len() as isize, empty.as_ptr() as _, self.vbotype);
            }
            let mut ptr = 0;
            for (i, s, t) in attrlist
            {
                gl::EnableVertexAttribArray(i);
                gl::VertexAttribPointer(i, s, t, gl::FALSE, size as i32, ptr as _);
                ptr += s * 4;
            }
            let err = gl::GetError();
            if err != gl::NO_ERROR
            {
                return Err(format!("Could not initialize vertex buffer {}: {}", vbo, err));
            }
        }
        let b: Box<dyn VertexBuffer> = Box::from(GlVertexBuffer::new(vbo, vao, self.vbotype, self.vcount, size as usize * self.vcount as usize));
        return Ok(b);
    }
}
