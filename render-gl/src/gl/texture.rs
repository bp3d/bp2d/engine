// Copyright (c) 2021, BlockProject 2D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 2D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::string::String;
use std::vec::Vec;
use std::boxed::Box;
use gl::types::GLuint;
use gl::types::GLint;
use gl::types::GLenum;
use gl::types::GLsizei;

use super::renderengine::GlRenderEngine;
use bp2d_render::engine::TextureFormat;
use bp2d_render::engine::TextureFilterMode;
use bp2d_render::engine::TextureWrapMode;
use bp2d_render::engine::TextureBuilder;
use bp2d_render::engine::Texture;
use bp2d_render::engine::Resource;

pub struct GlTexture
{
    texture: GLuint,
    wrap_s: TextureWrapMode,
    wrap_t: TextureWrapMode,
    filter_mode: TextureFilterMode,
    glinternal_format: GLenum,
    glformat: GLenum,
    gltype: GLenum,
    width: GLsizei,
    height: GLsizei,
    pixel_size: usize
}

impl GlTexture
{
    pub fn new(texture: GLuint, wrap_s: TextureWrapMode, wrap_t: TextureWrapMode, filter_mode: TextureFilterMode, format: TextureFormat, width: u32, height: u32) -> GlTexture
    {
        let (i, f, t) = texture_format_to_gl(format);
        return GlTexture
        {
            texture: texture,
            wrap_s: wrap_s,
            wrap_t: wrap_t,
            filter_mode: filter_mode,
            glinternal_format: i,
            glformat: f,
            gltype: t,
            width: width as GLsizei,
            height: height as GLsizei,
            pixel_size: get_pixel_size_bytes(format)
        };
    }
}

impl Resource for GlTexture
{
    fn lock(&mut self)
    {
        unsafe
        {
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
        }
    }

    fn free(self)
    {
        unsafe
        {
            gl::DeleteTextures(1, &self.texture);
        }
    }
}

impl Texture for GlTexture
{
    fn set_filter_mode(&mut self, mode: TextureFilterMode)
    {
        unsafe
        {
            match mode
            {
                TextureFilterMode::LinearMinMax =>
                {
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as GLint);
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as GLint);
                },
                TextureFilterMode::NearestMinMax =>
                {
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as GLint);
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as GLint);
                },
                TextureFilterMode::LinearMinNearestMax =>
                {
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as GLint);
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as GLint);
                },
                TextureFilterMode::NearestMinLinearMax =>
                {
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as GLint);
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as GLint);
                }
            };
            self.filter_mode = mode;
        }
    }

    fn set_wrap_mode(&mut self, w: TextureWrapMode, h: TextureWrapMode)
    {
        let s = match w
        {
            TextureWrapMode::Repeat => gl::REPEAT,
            TextureWrapMode::ClampToEdge => gl::CLAMP_TO_EDGE,
            TextureWrapMode::MirroredRepeat => gl::MIRRORED_REPEAT
        };
        let t = match h
        {
            TextureWrapMode::Repeat => gl::REPEAT,
            TextureWrapMode::ClampToEdge => gl::CLAMP_TO_EDGE,
            TextureWrapMode::MirroredRepeat => gl::MIRRORED_REPEAT
        };
        self.wrap_s = w;
        self.wrap_t = h;
        unsafe
        {
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, s as GLint);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, t as GLint);
        }
    }

    fn update(&mut self, data: &[u8]) -> Result<(), String>
    {
        if self.width as usize * self.height as usize * self.pixel_size != data.len()
        {
            return Err(format!("Invalid size of initial data, expected {} byte(s), got {} byte(s)", self.pixel_size * self.width as usize * self.height as usize, data.len()));
        }
        unsafe
        {
            gl::TexImage2D(gl::TEXTURE_2D, 0, self.glinternal_format as GLint, self.width, self.height, 0, self.glformat, self.gltype, data.as_ptr() as _);
        }
        self.set_wrap_mode(self.wrap_s, self.wrap_t);
        self.set_filter_mode(self.filter_mode);
        return Ok(());
    }
}

fn texture_format_to_gl(format: TextureFormat) -> (GLenum, GLenum, GLenum)
{
    return match format
    {
        TextureFormat::RGBA8 => (gl::RGBA, gl::RGBA, gl::UNSIGNED_BYTE),
        TextureFormat::RG8 => (gl::RG, gl::RG, gl::UNSIGNED_BYTE),
        TextureFormat::R8 => (gl::RED, gl::RED, gl::UNSIGNED_BYTE),
        TextureFormat::RGBAFloat => (gl::RGBA32F, gl::RGBA, gl::FLOAT),
        TextureFormat::RGFloat => (gl::RG32F, gl::RG, gl::FLOAT),
        TextureFormat::RFloat => (gl::R32F, gl::RED, gl::FLOAT)
    }
}

fn get_pixel_size_bytes(format: TextureFormat) -> usize
{
    return match format
    {
        TextureFormat::RGBA8 => 4,
        TextureFormat::RG8 => 2,
        TextureFormat::R8 => 1,
        TextureFormat::RGBAFloat => 16,
        TextureFormat::RGFloat => 8,
        TextureFormat::RFloat => 4
    }
}

pub struct GlTextureBuilder
{
    format: TextureFormat,
    wrap_mode_s: TextureWrapMode,
    wrap_mode_t: TextureWrapMode,
    filter_mode: TextureFilterMode,
    width: u32,
    height: u32,
    initial_data: Option<Vec<u8>>,
}

impl TextureBuilder for GlTextureBuilder
{    
    type TRenderEngine = GlRenderEngine;

    fn new(_: &mut Self::TRenderEngine) -> Self
    {
        return GlTextureBuilder
        {
            format: TextureFormat::RGBA8,
            wrap_mode_s: TextureWrapMode::ClampToEdge,
            wrap_mode_t: TextureWrapMode::ClampToEdge,
            filter_mode: TextureFilterMode::NearestMinMax,
            width: 0,
            height: 0,
            initial_data: None
        }
    }

    fn dynamic(self) -> Self
    {
        return self;
    }

    fn size(mut self, width: u32, height: u32) -> Self
    {
        self.width = width;
        self.height = height;
        return self;
    }

    fn format(mut self, format: TextureFormat) -> Self
    {
        self.format = format;
        return self;
    }

    fn filter_mode(mut self, filter_mode: TextureFilterMode) -> Self
    {
        self.filter_mode = filter_mode;
        return self;
    }

    fn wrap_mode(mut self, w: TextureWrapMode, h: TextureWrapMode) -> Self
    {
        self.wrap_mode_s = w;
        self.wrap_mode_t = h;
        return self;
    }

    fn data<I: IntoIterator<Item = u8>>(mut self, data: I) -> Self
    {
        self.initial_data = Some(data.into_iter().collect());
        return self;
    }

    fn build(self) -> Result<Box<dyn Texture>, String>
    {
        if self.width == 0 || self.height == 0
        {
            return Err(String::from("Cannot create a texture with 0 pixels"));
        }
        let size = get_pixel_size_bytes(self.format);
        if let Some(v) = &self.initial_data
        {
            if size * self.width as usize * self.height as usize != v.len()
            {
                return Err(format!("Invalid size of initial data, expected {} byte(s), got {} byte(s)", size * self.width as usize * self.height as usize, v.len()));
            }
        }
        let mut id: GLuint = 0;
        unsafe
        {
            gl::GenTextures(1, &mut id);
        }
        let mut texture = GlTexture::new(id, self.wrap_mode_s, self.wrap_mode_t, self.filter_mode, self.format, self.width, self.height);
        if let Some(data) = self.initial_data
        {
            texture.update(&data)?;
        }
        else
        {
            let empty: Vec<u8> = vec![0; self.width as usize * self.height as usize * size];
            texture.update(&empty)?;
        }
        let b: Box<dyn Texture> = Box::from(texture);
        return Ok(b);
    }
}
