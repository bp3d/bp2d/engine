// Copyright (c) 2021, BlockProject 2D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 2D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::collections::HashMap;
use std::string::String;
use std::vec::Vec;
use std::boxed::Box;
use std::ffi::CString;
use gl::types::GLuint;
use gl::types::GLint;
use gl::types::GLenum;
use map_in_place::MapVecInPlace;
use string_interner::StringInterner;
use string_interner::DefaultSymbol;

use super::renderengine::GlRenderEngine;
use bp2d_render::engine::ShaderStage;
use bp2d_render::engine::ShaderBuilder;
use bp2d_render::engine::Shader;
use bp2d_render::engine::Resource;

pub struct GlShader
{
    prog: GLuint,
    shaders: Vec<GLuint>,
    symcache: StringInterner,
    varcache: HashMap<DefaultSymbol, GLint>
}

impl GlShader
{
    pub fn new(prog: GLuint, shaders: Vec<GLuint>) -> GlShader
    {
        return GlShader
        {
            prog: prog,
            shaders: shaders,
            symcache: StringInterner::new(),
            varcache: HashMap::new()
        };
    }
}

impl Resource for GlShader
{
    fn lock(&mut self)
    {
        unsafe
        {
            gl::UseProgram(self.prog);
        }
    }

    fn free(self)
    {
        unsafe
        {
            for shader in self.shaders
            {
                gl::DeleteShader(shader);
            }
            gl::DeleteProgram(self.prog);
        }
    }
}

impl GlShader
{
    fn get_uniform_location(&mut self, name: &str) -> GLint
    {
        let sym = self.symcache.get_or_intern(name);
        if let Some(v) = self.varcache.get(&sym)
        {
            return *v;
        }
        else
        {
            unsafe
            {
                let loc = gl::GetUniformLocation(self.prog, name.as_ptr() as _);
                self.varcache.insert(sym, loc);
                return loc;
            }
        }
    }
}

impl Shader for GlShader
{
    fn setf(&mut self, name: &str, v: f32)
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform1f(loc, v);
        }
    }

    fn seti(&mut self, name: &str, v: i32)
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform1i(loc, v);
        }
    }

    fn setu(&mut self, name: &str, v: u32)
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform1ui(loc, v);
        }
    }

    fn set_vec2f(&mut self, name: &str, (v, v1): (f32, f32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform2f(loc, v, v1);
        }
    }

    fn set_vec3f(&mut self, name: &str, (v, v1, v2): (f32, f32, f32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform3f(loc, v, v1, v2);
        }
    }

    fn set_vec4f(&mut self, name: &str, (v, v1, v2, v3): (f32, f32, f32, f32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform4f(loc, v, v1, v2, v3);
        }
    }

    fn set_vec2i(&mut self, name: &str, (v, v1): (i32, i32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform2i(loc, v, v1);
        }
    }

    fn set_vec3i(&mut self, name: &str, (v, v1, v2): (i32, i32, i32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform3i(loc, v, v1, v2);
        }
    }

    fn set_vec4i(&mut self, name: &str, (v, v1, v2, v3): (i32, i32, i32, i32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform4i(loc, v, v1, v2, v3);
        }
    }

    fn set_vec2u(&mut self, name: &str, (v, v1): (u32, u32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform2ui(loc, v, v1);
        }
    }

    fn set_vec3u(&mut self, name: &str, (v, v1, v2): (u32, u32, u32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform3ui(loc, v, v1, v2);
        }
    }

    fn set_vec4u(&mut self, name: &str, (v, v1, v2, v3): (u32, u32, u32, u32))
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::Uniform4ui(loc, v, v1, v2, v3);
        }
    }

    fn set_mat2f(&mut self, name: &str, v: &[f32; 4])
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::UniformMatrix2fv(loc, 1, gl::FALSE, v.as_ptr());
        }
    }

    fn set_mat3f(&mut self, name: &str, v: &[f32; 9])
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::UniformMatrix3fv(loc, 1, gl::FALSE, v.as_ptr());
        }
    }

    fn set_mat4f(&mut self, name: &str, v: &[f32; 16])
    {
        let loc = self.get_uniform_location(name);
        unsafe
        {
            gl::UniformMatrix4fv(loc, 1, gl::FALSE, v.as_ptr());
        }
    }

    fn lock_texture_slot(&mut self, id: u8)
    {
        unsafe
        {
            gl::ActiveTexture(gl::TEXTURE0 + id as u32);
        }
    }
}

pub struct GlShaderBuilder
{
    pixel_stage: Option<Vec<String>>,
    geometry_stage: Option<Vec<String>>,
    vertex_stage: Option<Vec<String>>,
    attributes: Vec<String>
}

fn convert_strlist_to_cstrlist(source: Vec<String>) -> Vec<CString>
{
    let mut source_low_level = source.map_in_place(|s| s.into_bytes()); //1st pass: turn our vec into bytes
    let mut source_cstring = Vec::with_capacity(source_low_level.len());
    while !source_low_level.is_empty() //3rd pass: eventually turn our vec into raw CStrings
    {
        unsafe
        {
            source_cstring.push(CString::from_vec_unchecked(source_low_level.remove(0)));
        }
    }
    return source_cstring;
}

fn compile_shader(source: Vec<String>, stage: GLenum) -> Result<GLuint, String>
{
    let source_cstring = convert_strlist_to_cstrlist(source);
    let mut v: Vec<*const i8> = source_cstring.iter().map(|s| s.as_c_str().as_ptr()).collect();
    unsafe
    {
        let id = gl::CreateShader(stage);
        v.push(std::ptr::null());
        gl::ShaderSource(id, source_cstring.len() as _, v.as_ptr(), std::ptr::null());
        gl::CompileShader(id);
        let mut val: GLint = 0;
        gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut val);
        if val == gl::TRUE as i32
        {
            return Ok(id);
        }
        gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut val);
        let mut v: Vec<u8> = vec![0; val as usize + 1]; //Ensure we have always have a null terminator
        gl::GetShaderInfoLog(id, val, std::ptr::null_mut(), v.as_mut_ptr() as _);
        let s = CString::from_vec_unchecked(v);
        match s.into_string()
        {
            Ok(s) => return Err(format!("Error compiling shader: {}", s)),
            Err(_) => return Err(String::from("Error compiling shader: failed to encode shader compiler log"))
        };
    }
}

impl ShaderBuilder for GlShaderBuilder
{
    type TRenderEngine = GlRenderEngine;

    fn new(_: &mut Self::TRenderEngine) -> Self
    {
        return GlShaderBuilder
        {
            pixel_stage: None,
            geometry_stage: None,
            vertex_stage: None,
            attributes: Vec::new()
        };
    }

    fn add_stage(mut self, stage: ShaderStage, source: Vec<String>) -> Self
    {
        match stage
        {
            ShaderStage::Geometry => self.geometry_stage = Some(source),
            ShaderStage::Pixel => self.pixel_stage = Some(source),
            ShaderStage::Vertex => self.vertex_stage = Some(source)
        }
        return self;
    }

    fn add_attribute(mut self, name: &str) -> Self
    {
        self.attributes.push(String::from(name));
        return self;
    }

    fn build(self) -> Result<Box<dyn Shader>, String>
    {
        unsafe
        {
            let id = gl::CreateProgram();
            let mut shaders = Vec::new();
            if let Some(vertex) = self.vertex_stage
            {
                let shader = compile_shader(vertex, gl::VERTEX_SHADER)?;
                gl::AttachShader(id, shader);
                shaders.push(shader);
            }
            if let Some(geometry) = self.geometry_stage
            {
                let shader = compile_shader(geometry, gl::GEOMETRY_SHADER)?;
                gl::AttachShader(id, shader);
                shaders.push(shader);
            }
            if let Some(pixel) = self.pixel_stage
            {
                let shader = compile_shader(pixel, gl::FRAGMENT_SHADER)?;
                gl::AttachShader(id, shader);
                shaders.push(shader);
            }
            let mut val: GLint = 0;
            gl::GetProgramiv(id, gl::LINK_STATUS, &mut val);
            if val == gl::TRUE as GLint
            {
                let b: Box<dyn Shader> = Box::from(GlShader::new(id, shaders));
                return Ok(b);
            }
            gl::GetProgramiv(id, gl::INFO_LOG_LENGTH, &mut val);
            let mut v: Vec<u8> = vec![0; val as usize + 1]; //Ensure we have always have a null terminator
            gl::GetProgramInfoLog(id, val, std::ptr::null_mut(), v.as_mut_ptr() as _);
            let s = CString::from_vec_unchecked(v);
            match s.into_string()
            {
                Ok(s) => return Err(format!("Error linking shader: {}", s)),
                Err(_) => return Err(String::from("Error linking shader: failed to encode shader linker log"))
            };
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn strlist_to_cstrlist()
    {
        let source = vec!(
            String::from("int main()"),
            String::from("{"),
            String::from("   return (0);"),
            String::from("}"),
        );
        let low_level = convert_strlist_to_cstrlist(source);
        println!("{:?}", low_level);
        assert_eq!(low_level, vec!(
            CString::new("int main()").unwrap(),
            CString::new("{").unwrap(),
            CString::new("   return (0);").unwrap(),
            CString::new("}").unwrap(),
        ));
    }
}
