// Copyright (c) 2021, BlockProject 2D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 2D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::ops::Add;
use std::ops::Sub;
use std::ops::Mul;
use std::ops::Div;
use std::ops::AddAssign;
use std::ops::SubAssign;
use std::ops::MulAssign;
use std::ops::DivAssign;
use derive_more::Add;
use derive_more::Mul;
use derive_more::Sub;
use derive_more::Div;
use derive_more::AddAssign;
use derive_more::MulAssign;
use derive_more::SubAssign;
use derive_more::DivAssign;

use crate::num::Sqrt;

pub trait Item: Sized + Copy + Add<Output = Self> + Sub<Output = Self> + Mul<Output = Self> + Div<Output = Self> + AddAssign + SubAssign + MulAssign + DivAssign + Sqrt {}
impl <T: Sized + Copy + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + AddAssign + SubAssign + MulAssign + DivAssign + Sqrt> Item for T {}

#[mul(forward)]
#[mul_assign(forward)]
#[div(forward)]
#[div_assign(forward)]
#[derive(Clone, Copy, Debug, Add, Mul, Div, Sub, AddAssign, MulAssign, SubAssign, DivAssign)]
pub struct Vec2<T: Item>
{
    pub x: T,
    pub y: T
}

impl <T: Item> Vec2<T>
{
    pub fn new(x: T, y: T) -> Vec2<T>
    {
        return Vec2
        {
            x: x,
            y: y
        };
    }

    pub fn length_squarred(&self) -> T
    {
        return self.x * self.x + self.y * self.y;
    }

    pub fn length(&self) -> T
    {
        return self.length_squarred().sqrt();
    }

    pub fn distance<T1: AsRef<Vec2<T>>>(&self, to: T1) -> T
    {
        return (*to.as_ref() - *self).length();
    }

    pub fn distance_squarred<T1: AsRef<Vec2<T>>>(&self, to: T1) -> T
    {
        return (*to.as_ref() - *self).length_squarred();
    }
}

impl <T: Item> Mul<T> for Vec2<T>
{
    type Output = Vec2<T>;

    fn mul(self, item: T) -> Vec2<T>
    {
        return Vec2
        {
            x: self.x * item,
            y: self.y * item
        };
    }
}

impl <T: Item> MulAssign<T> for Vec2<T>
{
    fn mul_assign(&mut self, item: T)
    {
        self.x *= item;
        self.y *= item;
    }
}

impl <T: Item> Div<T> for Vec2<T>
{
    type Output = Vec2<T>;

    fn div(self, item: T) -> Vec2<T>
    {
        return Vec2
        {
            x: self.x / item,
            y: self.y / item
        };
    }
}

impl <T: Item> DivAssign<T> for Vec2<T>
{
    fn div_assign(&mut self, item: T)
    {
        self.x /= item;
        self.y /= item;
    }
}

#[mul(forward)]
#[mul_assign(forward)]
#[div(forward)]
#[div_assign(forward)]
#[derive(Clone, Copy, Debug, Add, Mul, Div, Sub, AddAssign, MulAssign, SubAssign, DivAssign)]
pub struct Vec3<T: Item>
{
    pub x: T,
    pub y: T,
    pub z: T
}

impl <T: Item> Vec3<T>
{
    pub fn new(x: T, y: T, z: T) -> Vec3<T>
    {
        return Vec3
        {
            x: x,
            y: y,
            z: z
        };
    }

    pub fn length_squarred(&self) -> T
    {
        return self.x * self.x + self.y * self.y + self.z * self.z;
    }

    pub fn length(&self) -> T
    {
        return self.length_squarred().sqrt();
    }

    pub fn distance<T1: AsRef<Vec3<T>>>(&self, to: T1) -> T
    {
        return (*to.as_ref() - *self).length();
    }

    pub fn distance_squarred<T1: AsRef<Vec3<T>>>(&self, to: T1) -> T
    {
        return (*to.as_ref() - *self).length_squarred();
    }
}

impl <T: Item> Mul<T> for Vec3<T>
{
    type Output = Vec3<T>;

    fn mul(self, item: T) -> Vec3<T>
    {
        return Vec3
        {
            x: self.x * item,
            y: self.y * item,
            z: self.z * item
        };
    }
}

impl <T: Item> MulAssign<T> for Vec3<T>
{
    fn mul_assign(&mut self, item: T)
    {
        self.x *= item;
        self.y *= item;
        self.z *= item;
    }
}

impl <T: Item> Div<T> for Vec3<T>
{
    type Output = Vec3<T>;

    fn div(self, item: T) -> Vec3<T>
    {
        return Vec3
        {
            x: self.x / item,
            y: self.y / item,
            z: self.z / item
        };
    }
}

impl <T: Item> DivAssign<T> for Vec3<T>
{
    fn div_assign(&mut self, item: T)
    {
        self.x /= item;
        self.y /= item;
        self.z /= item;
    }
}
