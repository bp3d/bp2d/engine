// Copyright (c) 2021, BlockProject 2D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 2D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::vec::Vec;

use crate::ecs::interface::ComponentAllocator;
use crate::ecs::interface::Pipeline;

pub struct ComponentPool<TAllocator: ComponentAllocator>
{
    pool: Vec<TAllocator::TComponent>
}

impl <TAllocator: ComponentAllocator> ComponentPool<TAllocator>
{
    pub fn new() -> ComponentPool<TAllocator>
    {
        return ComponentPool
        {
            pool: Vec::new()
        };
    }

    pub fn alloc(&mut self) -> usize
    {
        let size = self.pool.len();
        self.pool.push(TAllocator::new());
        return size;
    }

    pub fn free(&mut self, id: usize) -> bool
    {
        if id >= self.pool.len()
        {
            return false;
        }
        self.pool.remove(id);
        return true;
    }

    pub fn get_mut(&mut self, id: usize) -> Option<&mut TAllocator::TComponent>
    {
        return self.pool.get_mut(id);
    }
}

pub struct Scene<TPipeline: Pipeline>
{
    pipeline: TPipeline,
}

impl <TPipeline: Pipeline> Scene<TPipeline>
{
    pub fn new(pipeline: TPipeline) -> Scene<TPipeline>
    {
        return Scene
        {
            pipeline: pipeline
        };
    }
}
